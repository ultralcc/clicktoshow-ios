//
//  ViewController.swift
//  ClickToShow
//
//  Created by 訪客使用者 on 2017/11/14.
//  Copyright © 2017年 LCC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    var count = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.countLabel.text = String(self.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func clickToShow(_ sender: Any) {
        self.welcomeLabel.text = "Hello"
        count += 1
        self.countLabel.text = String(self.count)
    }
    
    @IBAction func reset(_ sender: Any) {
        count = 0
        self.countLabel.text = String(self.count)
        self.welcomeLabel.text = "Welcome"
    }
    
}

